package unittests

import (
	"testing"

	"gitlab.com/fastogt/gofastocloud_base"

	"github.com/stretchr/testify/assert"
)

func TestParse(t *testing.T) {
	data := []byte("{\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32600, \"message\": \"Invalid Request\"}, \"id\": \"123\"}")
	req, resp := gofastocloud_base.ParseResponseOrRequest(data)

	//Assert
	assert.Nil(t, req)

	id := gofastocloud_base.NewStringRPCId("123")

	assert.Equal(t, *resp.Id, *id)
	assert.True(t, resp.IsValid())
	assert.True(t, resp.IsError())
	assert.False(t, resp.IsMessage())
	assert.Nil(t, resp.Result)
	err := resp.Error
	assert.Equal(t, err.Code, -32600)
	assert.Equal(t, err.Message, "Invalid Request")
}
