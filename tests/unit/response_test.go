package unittests

import (
	"encoding/json"
	"testing"

	"gitlab.com/fastogt/gofastocloud_base"

	"github.com/stretchr/testify/assert"
)

func TestResponse(t *testing.T) {
	//Arrange
	result := json.RawMessage("{\"some_key\": \"some_value\"}")
	response := gofastocloud_base.NewResponseMessage(gofastocloud_base.NewStringRPCId("1213"), &result)

	var err = gofastocloud_base.NewRPCError(202, "some_message")
	responseErr := gofastocloud_base.NewResponseError(gofastocloud_base.NewStringRPCId("1213"), err)

	//Assert
	assert.True(t, response.IsValid())
	assert.False(t, response.IsError())
	assert.True(t, response.IsMessage())
	assert.Equal(t, response.Result, &result)

	assert.True(t, responseErr.IsValid())
	assert.True(t, responseErr.IsError())
	assert.False(t, responseErr.IsMessage())
	assert.Nil(t, responseErr.Result)
}
