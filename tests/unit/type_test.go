package unittests

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fastogt/gofastocloud_base"
)

func TestUnmarshalJSON(t *testing.T) {
	var operation gofastocloud_base.OperationSystem

	dataTest := []byte(`{"name":"user","version":"2.0","arch":"user","ram_total":123,"ram_free":123}`)
	err := json.Unmarshal(dataTest, &operation)
	assert.True(t, err == nil)
	assert.Equal(t, operation.Name, "user")
	assert.Equal(t, operation.RamTotal, int64(123))

	dataTest2 := []byte(`{"ram_total":123,"ram_free":123}`)
	err = json.Unmarshal(dataTest2, &operation)
	assert.False(t, err == nil)

	dataTest3 := []byte(`{"name":"","version":"2.0","arch":"user","ram_total":123,"ram_free":123}`)
	err = json.Unmarshal(dataTest3, &operation)
	assert.False(t, err == nil)

	var lc gofastocloud_base.LicenseKey
	lc = "32"
	assert.False(t, lc.IsValid())

	lc = "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
	assert.True(t, lc.IsValid())
}
