package gofastocloud_base

import "errors"

var ErrNotConnected = errors.New("not connected")
var ErrInvalidFormat = errors.New("invalid m3u file format")
