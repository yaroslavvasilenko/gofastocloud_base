module gitlab.com/fastogt/gofastocloud_base

go 1.13

require (
	github.com/stretchr/testify v1.7.1
	gitlab.com/fastogt/gofastogt v1.2.4
)
